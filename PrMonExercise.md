# Using PRocess MONitor (prmon)  

[PRocess MONitor (prmon)](https://github.com/HSF/prmon) is a small stand alone program that can monitor the resource consumption of a process and its children.
For standalone use, you can set it up via `lsetup` as:

```bash
lsetup prmon
```

Note that nightlies and number releases that originate from the master branch already include `prmon`.
Therefore, there is no need for any extra setup there.
The main executable is called `prmon` and the main script that produces plots from the text output is called `prmon_plot.py`.
You can get information about their usage by simply calling them with the `-h` argument, e.g.:

```bash
$ prmon -h
prmon is a process monitor program that records runtime data
from a process and its children, writing time stamped values
for resource consumption into a logfile and a JSON summary
format when the process exits.

Options:
[--pid, -p PID]           Monitored process ID
[--filename, -f FILE]     Filename for detailed stats (default prmon.txt)
[--json-summary, -j FILE] Filename for JSON summary (default prmon.json)
[--interval, -i TIME]     Seconds between samples (default 30)
[--suppress-hw-info, -s]  Disable hardware information (default false)
[--units, -u]             Add units information to JSON file (default false)
[--netdev, -n dev]        Network device to monitor (can be given
                          multiple times; default ALL devices)
[--disable, -d mon]       Disable monitor component
                          (can be given multiple times);
                          all monitors enabled by default
                          Special name '[~]all' sets default state
[--] prog [arg] ...       Instead of monitoring a PID prmon will
                          execute the given program + args and
                          monitor this (must come after other 
                          arguments)

One of --pid or a child program must be given (but not both)

Monitors available:
 - countmon : Monitor number of processes and threads
 - cpumon : Monitor cpu time used
 - iomon : Monitor input and output activity
 - memmon : Monitor memory usage
 - netmon : Monitor network activity (device level)
 - nvidiamon : Monitor NVIDIA GPU activity
 - wallmon : Monitor wallclock time

More information: https://github.com/HSF/prmon
```

There are two main outputs: 

* A plain text file that contains the statistics written at every `interval`. The first line provides the column names.
* A JSON summary file that contains the maximum and average statistics. This file is rewritten at every `interval` with the most up-to-date values.

We run `prmon` as part of the official job transforms. Therefore, every time you run an ATLAS transfrom, i.e. `Reco_tf.py`, 
you should end up with these two files in your run directory, with the naming convention of `prmon.full.{step_name}` and `prmon.summary.{step_name}`, respectively.

For demonstration purposes, let us use a generic output (which is actually coming from an actual, albeit old, ATLAS workload) that you can find [here](https://raw.githubusercontent.com/HSF/prmon/master/example-plots/prmon.txt).
Now, let's make a few plots using the `prmon_plot.py` script:

```bash
$ prmon_plot.py -h
usage: prmon_plot.py [-h] [--input INPUT] [--xvar XVAR]
                     [--xunit [{SEC,MIN,HOUR,B,KB,MB,GB,1}]] [--yvar YVAR]
                     [--yunit [{SEC,MIN,HOUR,B,KB,MB,GB,1}]] [--stacked]
                     [--diff] [--otype [{png,pdf,svg}]]

Configurable plotting script

optional arguments:
  -h, --help            show this help message and exit
  --input INPUT         PrMon TXT output that will be used as input
  --xvar XVAR           name of the variable to be plotted in the x-axis
  --xunit [{SEC,MIN,HOUR,B,KB,MB,GB,1}]
                        unit of the variable to be plotted in the x-axis
  --yvar YVAR           name(s) of the variable to be plotted in the y-axis
                        (comma seperated list is accepted)
  --yunit [{SEC,MIN,HOUR,B,KB,MB,GB,1}]
                        unit of the variable to be plotted in the y-axis
  --stacked             stack plots if specified
  --diff                plot the ratio of the discrete differences of the
                        elements for yvars and xvars if specified
  --otype [{png,pdf,svg}]
                        format of the output image
```

You can make the plot of memory usage as a function of wall-time using:

```bash
prmon_plot.py --input prmon.txt --xvar wtime --yvar vmem,pss,rss,swap
```

where as the effective CPU usage as a function of wall-time using:

```bash
prmon_plot.py --input prmon.txt --xvar wtime --yvar utime,stime --yunit SEC --diff --stacked
```

Give it a shot and see how it goes. By the way, remember that you can use `prmon` for any application that you want!
