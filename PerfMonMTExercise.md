# Using PerfMonMT in Athena jobs to time an algorithm

In this exercise we'll see how to enable PerfMonMT in Athena jobs.
Let us get started by setting up a nigthly:

```bash
lsetup "asetup Athena,master,latest"
```

Now let's take a look at [PerfMonTests/test_perfMonMTSvc_mt1.py](https://gitlab.cern.ch/atlas/athena/-/blob/fa068ec73f3e0263ff649cd8fb2e9816d565ead4/Control/PerformanceMonitoring/PerfMonTests/test/test_perfMonMTSvc_mt1.py) to see how one can set up PerfMonMT in a CA based job.
The relevant flags for PerfMonMT are:

```python
ConfigFlags.PerfMon.doFullMonMT = True
ConfigFlags.PerfMon.OutputJSON = 'perfmonmt_test.json'
```

where we enable the detailed monitoring and specify the output file name.
Then all one has to do is to add the service to the job with:

```python
cfg.merge(PerfMonMTSvcCfg(ConfigFlags))
```

and run it.
But before doing that, let's take a quick look at the algorithms that run in this job.
These are: [CpuCruncherAlg](https://gitlab.cern.ch/atlas/athena/-/blob/fa068ec73f3e0263ff649cd8fb2e9816d565ead4/Control/PerformanceMonitoring/PerfMonTests/src/PerfMonTestCpuCruncherAlg.cxx) and [LeakyAlg](https://gitlab.cern.ch/atlas/athena/-/blob/master/Control/PerformanceMonitoring/PerfMonTests/src/PerfMonTestLeakyAlg.cxx).
The former simply keeps the CPU busy for a fixed amount of time and the latter keeps allocating a fixed number of integers per event.
You can simply run this test as (after creating a local copy):

```bash
./test_perfMonMTSvc_mt1.py
```

Upon sucessful run, you should see a detailed report from PerfMonMTSvc, which should include (among other things):

```
PerfMonMTSvc                                                      INFO =======================================================================================
PerfMonMTSvc                                                      INFO                                 Snapshots Summary                                      
PerfMonMTSvc                                                      INFO =======================================================================================
PerfMonMTSvc                                                      INFO Step         dCPU [s]    dWall [s]   <CPU>  dVmem [kB] dRss [kB]  dPss [kB]  dSwap [kB]
PerfMonMTSvc                                                      INFO ---------------------------------------------------------------------------------------
PerfMonMTSvc                                                      INFO Configure    1.74        4.795       0.36   794216     286932     285076     0
PerfMonMTSvc                                                      INFO Initialize   0.06        0.116       0.52   24996      11764      11736      0
PerfMonMTSvc                                                      INFO FirstEvent   0.12        0.12        1.00   69636      2644       2640       0
PerfMonMTSvc                                                      INFO Execute      10.06       10.035      1.00   0          4012       4012       0
PerfMonMTSvc                                                      INFO Finalize     0           0.029       0.00   0          56         52         0
PerfMonMTSvc                                                      INFO ***************************************************************************************
PerfMonMTSvc                                                      INFO Number of events processed:        100 
PerfMonMTSvc                                                      INFO CPU usage per event [ms]:          102 
PerfMonMTSvc                                                      INFO Events per second:                 9.847 
PerfMonMTSvc                                                      INFO CPU utilization efficiency [%]:    79 
PerfMonMTSvc                                                      INFO ***************************************************************************************
PerfMonMTSvc                                                      INFO Max Vmem:                          940.02 MB 
PerfMonMTSvc                                                      INFO Max Rss:                           298.55 MB 
PerfMonMTSvc                                                      INFO Max Pss:                           296.71 MB 
PerfMonMTSvc                                                      INFO Max Swap:                          0.00 KB 
PerfMonMTSvc                                                      INFO ***************************************************************************************
PerfMonMTSvc                                                      INFO Leak estimate per event Vmem:      0.00 KB 
PerfMonMTSvc                                                      INFO Leak estimate per event Pss:       40.00 KB 
PerfMonMTSvc                                                      INFO   >> Estimated using the last 3 measurements from the Event Level Monitoring
PerfMonMTSvc                                                      INFO   >> Events prior to the first 25 are omitted...
PerfMonMTSvc                                                      INFO =======================================================================================
PerfMonMTSvc                                                      INFO                                   System Information                                   
PerfMonMTSvc                                                      INFO =======================================================================================
PerfMonMTSvc                                                      INFO CPU Model:                         Intel(R) Xeon(R) CPU E5-2667 v2 @ 3.30GHz 25600 KB 
PerfMonMTSvc                                                      INFO Number of Available Cores:         16 
PerfMonMTSvc                                                      INFO Total Memory:                      31.21 GB 
PerfMonMTSvc                                                      INFO =======================================================================================
PerfMonMTSvc                                                      INFO                                Environment Information                                 
PerfMonMTSvc                                                      INFO =======================================================================================
PerfMonMTSvc                                                      INFO Malloc Library:                    libc.so.6 
PerfMonMTSvc                                                      INFO Math Library:                      libm.so.6 
PerfMonMTSvc                                                      INFO =======================================================================================
```

This is in agreement with the settings of the algorithms, where `CpuCruncherAlg` should use `100 +/- 1 ms/event` and `LeakyAlg` should "leak" `40 KB/event`. Note that there might be a tiny bit of overhead in the CPU usage because `CpuCruncherAlg` is not the only thing that executes in this job.
You can also take a look at the detailed usage statistics (`Component Level Monitoring`) to see the break-down of these by components.

Now, you can pass the resulting JSON file, namely `perfmonmt_test.json` to the helper scripts:
-  [PerfMonAna/perfmonmt-printer.py](https://gitlab.cern.ch/atlas/athena/-/blob/fa068ec73f3e0263ff649cd8fb2e9816d565ead4/Control/PerformanceMonitoring/PerfMonAna/bin/perfmonmt-printer.py)
-  [PerfMonAna/perfmonmt-plotter.py](https://gitlab.cern.ch/atlas/athena/-/blob/fa068ec73f3e0263ff649cd8fb2e9816d565ead4/Control/PerformanceMonitoring/PerfMonAna/bin/perfmonmt-plotter.py)
-  [PerfMonAna/perfmonmt-refit.py](https://gitlab.cern.ch/atlas/athena/-/blob/fa068ec73f3e0263ff649cd8fb2e9816d565ead4/Control/PerformanceMonitoring/PerfMonAna/bin/perfmonmt-refit.py)

to produce some plots, reprint the tables (by changing the ordering), and/or refit the memory measurements (to see how the slope behaves in different slices of events). For example, in order to print the component-level metrics only in the event-loop (except first-event) reversed ordered in cumulative memory allocations, simply do:

```bash
perfmonmt-printer.py -l ComponentLevel -e -o malloc -i perfmonmt_test.json.tar.gz
```

Now let's come back to the output above. If you look at the event-loop metrics, `Execute` under `Snapshot Summary` section, you'll see that on average we're using a single CPU, `<CPU>` column. Now update your example to use 8 threads instead by updating the following lines:

```python
ConfigFlags.Concurrency.NumThreads = 8
```

and re-run your example. Did things improve? Check how many CPUs your job is using and see if the event throughput, `Events per second`, improved at all.
Unfortunately, although you increased the number of threads and the concurrent events in-flight, nothing improved.
This is due to the fact that both `CpuCruncherAlg` and `LeakyAlg` are non-reentrant. This means there is only one instance for each and threads need to wait for one another to execute them.

The best way to improve the situation is to make your algorithm(s) reentrant. Doing so will also give you the chance to review the code, clean it up etc.
Especially if you're starting from scratch, never inherit from the legacy algorithm base class unless you have a good reason. 
However, for the sake of demonstration we'll do something else. Let's take a look at [PerfMonTests/test_perfMonMTSvc_mt8.py](https://gitlab.cern.ch/atlas/athena/-/blob/fa068ec73f3e0263ff649cd8fb2e9816d565ead4/Control/PerformanceMonitoring/PerfMonTests/test/test_perfMonMTSvc_mt8.py) and adjust your example to match that and run. How do things look like now? `PerfMonMTSvc` output should look something like this: 

```
PerfMonMTSvc                                                      INFO =======================================================================================
PerfMonMTSvc                                                      INFO                                 Event Level Monitoring                                 
PerfMonMTSvc                                                      INFO         (Only the first 10 and the last measurements are explicitly printed)
PerfMonMTSvc                                                      INFO =======================================================================================
PerfMonMTSvc                                                      INFO Event           CPU [s]     Wall [s]    Vmem [kB]   Rss [kB]    Pss [kB]    Swap [kB]
PerfMonMTSvc                                                      INFO ---------------------------------------------------------------------------------------
PerfMonMTSvc                                                      INFO 1               1.80        5.23        1450076     302016      300157      0
PerfMonMTSvc                                                      INFO 2               1.93        5.34        1450076     302844      300985      0
PerfMonMTSvc                                                      INFO 10              2.74        5.45        1450076     317144      315285      0
PerfMonMTSvc                                                      INFO 25              3.56        5.56        1450076     317780      315921      0
PerfMonMTSvc                                                      INFO 50              6.78        5.96        1450076     318792      316933      0
PerfMonMTSvc                                                      INFO 100             11.66       6.58        1450076     320836      318977      0
PerfMonMTSvc                                                      INFO =======================================================================================
PerfMonMTSvc                                                      INFO                                 Snapshots Summary                                      
PerfMonMTSvc                                                      INFO =======================================================================================
PerfMonMTSvc                                                      INFO Step         dCPU [s]    dWall [s]   <CPU>  dVmem [kB] dRss [kB]  dPss [kB]  dSwap [kB]
PerfMonMTSvc                                                      INFO ---------------------------------------------------------------------------------------
PerfMonMTSvc                                                      INFO Configure    1.72        4.124       0.42   794224     286932     285100     0
PerfMonMTSvc                                                      INFO Initialize   0.05        0.074       0.68   25032      11800      11776      0
PerfMonMTSvc                                                      INFO FirstEvent   0.13        0.122       1.07   557088     2920       2917       0
PerfMonMTSvc                                                      INFO Execute      10.06       1.351       7.45   0          18736      18736      0
PerfMonMTSvc                                                      INFO Finalize     0.01        0.17        0.06   0          32         28         0
PerfMonMTSvc                                                      INFO ***************************************************************************************
PerfMonMTSvc                                                      INFO Number of events processed:        100 
PerfMonMTSvc                                                      INFO CPU usage per event [ms]:          102 
PerfMonMTSvc                                                      INFO Events per second:                 67.889 
PerfMonMTSvc                                                      INFO CPU utilization efficiency [%]:    78 
PerfMonMTSvc                                                      INFO ***************************************************************************************
PerfMonMTSvc                                                      INFO Max Vmem:                          1.38 GB 
PerfMonMTSvc                                                      INFO Max Rss:                           313.32 MB 
PerfMonMTSvc                                                      INFO Max Pss:                           311.50 MB 
PerfMonMTSvc                                                      INFO Max Swap:                          0.00 KB 
PerfMonMTSvc                                                      INFO ***************************************************************************************
PerfMonMTSvc                                                      INFO Leak estimate per event Vmem:      0.00 KB 
PerfMonMTSvc                                                      INFO Leak estimate per event Pss:       40.00 KB 
PerfMonMTSvc                                                      INFO   >> Estimated using the last 3 measurements from the Event Level Monitoring
PerfMonMTSvc                                                      INFO   >> Events prior to the first 25 are omitted...
PerfMonMTSvc                                                      INFO =======================================================================================
PerfMonMTSvc                                                      INFO                                   System Information                                   
PerfMonMTSvc                                                      INFO =======================================================================================
PerfMonMTSvc                                                      INFO CPU Model:                         Intel(R) Xeon(R) CPU E5-2667 v2 @ 3.30GHz 25600 KB 
PerfMonMTSvc                                                      INFO Number of Available Cores:         16 
PerfMonMTSvc                                                      INFO Total Memory:                      31.21 GB 
PerfMonMTSvc                                                      INFO =======================================================================================
PerfMonMTSvc                                                      INFO                                Environment Information                                 
PerfMonMTSvc                                                      INFO =======================================================================================
PerfMonMTSvc                                                      INFO Malloc Library:                    libc.so.6 
PerfMonMTSvc                                                      INFO Math Library:                      libm.so.6 
PerfMonMTSvc                                                      INFO =======================================================================================
```

Now check how many CPUs your job is using in the event-loop and see if the event throughput improved w.r.t. what you had before when you asked for 8 threads.
This setup is almost identical to what you had before but now we set the cardinality (i.e. number of copies) of the algorithms to match the number of threads/concurrent events. As such, we drastically improved the throughput but at the expense of using a bit more memory.

After all these exercises, pick your favorite workflow, enable `PerfMonMT` and take it for a spin. If you have any questions/bug reports/suggestions get in touch with the SPOT team. We'll be happy to hear from you!
