# Analyzing a Valgrind output to find memory leaks

In this brief exercise we're going to use the valgrind output of a problematic test.
The output we're going to use can be found directly at 
[this link](https://atlaspmb.web.cern.ch/atlaspmb/PerformanceTutorial201909/valgrind.out)
as well as here:

```
/afs/cern.ch/user/a/amete/public/PerformanceTutorial201909/valgrind.out.tar.gz
```

For the curios reader, the full command that's used to obtain this result is
as follows (using the nightly of master/2019-08-06T2130, which is now obsolete).
Please remember that most of these steps are redundant these days as mentioned in the presentation.
The command is provided only for completeness.

```bash
# Get some useful suppress files
get_files oracleDB.supp
get_files valgrindRTT.supp
get_files newSuppressions.supp

# Valgrind
Reco_tf.py \
--inputBSFile /atlas/scratch0/amete/ATR-18638/data17_13TeV.00326446.physics_Main.daq.RAW._lb0154._SFO-1._0001.data \
--maxEvents 25  \
--autoConfiguration 'everything' \
--conditionsTag  'CONDBR2-BLKPA-2017-10' \
--geometryVersion  'ATLAS-R2-2016-01-00-01'  \
--outputAODFile  'myAOD.pool.root' \
--athenaopts ' --stdcmalloc --config-only=r2a.pkl' \
--postExec 'include("RecExCommon/ValgrindTweaks.py")' \
--ignoreErrors 'True' \
--steering doRAWtoALL;

# Get a later version of valgrind from LCG_95
export  VALGRIND_LIB="/cvmfs/atlas-nightlies.cern.ch/repo/sw/master/sw/lcg/releases/LCG_95/valgrind/3.13.0/x86_64-centos7-gcc8-opt/lib/valgrind"
/cvmfs/atlas-nightlies.cern.ch/repo/sw/master/sw/lcg/releases/LCG_95/valgrind/3.13.0/x86_64-centos7-gcc8-opt/bin/valgrind \
--show-possibly-lost=no \
--smc-check=all \
--tool=memcheck \
--leak-check=full \
--suppressions=newSuppressions.supp \
--suppressions=${ROOTSYS}/etc/valgrind-root.supp \
--suppressions=oracleDB.supp \
--suppressions=valgrindRTT.supp  \
--num-callers=30 \
--track-origins=yes `which python` `which athena.py` --stdcmalloc r2a.pkl >& valgrind.out;
```

If you scroll all the way down, you'll see the summary report of the leak check:

```
==7535== LEAK SUMMARY:
==7535==    definitely lost: 10,311,572 bytes in 40,030 blocks
==7535==    indirectly lost: 90,775,024 bytes in 633,215 blocks
==7535==      possibly lost: 18,618,050 bytes in 219,084 blocks
==7535==    still reachable: 688,731,389 bytes in 1,249,955 blocks
==7535==                       of which reachable via heuristic:
==7535==                         newarray           : 478,840 bytes in 745 blocks
==7535==                         multipleinheritance: 229,136 bytes in 2,270 blocks
==7535==         suppressed: 13,998,835 bytes in 152,417 blocks
==7535== Reachable blocks (those to which a pointer was found) are not shown.
==7535== To see them, rerun with: --leak-check=full --show-leak-kinds=all
==7535== 
==7535== For lists of detected and suppressed errors, rerun with: -s
==7535== ERROR SUMMARY: 4757 errors from 1256 contexts (suppressed: 704631 from 16104)
```

The first bit is the process id, which is irrelevant for us.
In most cases we concentrate on the `definitely lost` part since this is the case
in which no pointer to the memory block can be found (i.e. this memory is gone for good).

By default Valgrind orders the leaks such that the largest is the last one on the list.
Therefore, if you simply look above you'll see:

```
==7535== 24,103,544 (4,577,664 direct, 19,525,880 indirect) bytes in 5,109 blocks are definitely lost in loss record 123,085 of 123,088
==7535==    at 0x4C2AA02: operator new(unsigned long) (vg_replace_malloc.c:344)
==7535==    by 0x6F4392BF: Trk::TrkAmbiguitySolver::execute() (TrkAmbiguitySolver.cxx:60)
==7535==    by 0x22CFF169: Gaudi::details::LegacyAlgorithmAdapter::execute(EventContext const&) const (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/GAUDI/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libGaudiPythonLib.so)
==7535==    by 0x1E26CC36: Gaudi::Algorithm::sysExecute(EventContext const&) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/GAUDI/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libGaudiKernel.so)
==7535==    by 0x5832DFD8: AthSequencer::executeAlgorithm(Gaudi::Algorithm*, EventContext const&, bool volatile&, bool volatile&) const (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/Athena/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libGaudiSequencer.so)
==7535==    by 0x5832E448: AthSequencer::execute(EventContext const&) const (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/Athena/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libGaudiSequencer.so)
==7535==    by 0x1E26CC36: Gaudi::Algorithm::sysExecute(EventContext const&) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/GAUDI/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libGaudiKernel.so)
==7535==    by 0x5832DFD8: AthSequencer::executeAlgorithm(Gaudi::Algorithm*, EventContext const&, bool volatile&, bool volatile&) const (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/Athena/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libGaudiSequencer.so)
==7535==    by 0x5832E448: AthSequencer::execute(EventContext const&) const (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/Athena/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libGaudiSequencer.so)
==7535==    by 0x1E26CC36: Gaudi::Algorithm::sysExecute(EventContext const&) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/GAUDI/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libGaudiKernel.so)
==7535==    by 0x5832DFD8: AthSequencer::executeAlgorithm(Gaudi::Algorithm*, EventContext const&, bool volatile&, bool volatile&) const (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/Athena/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libGaudiSequencer.so)
==7535==    by 0x5832E448: AthSequencer::execute(EventContext const&) const (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/Athena/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libGaudiSequencer.so)
==7535==    by 0x1E26CC36: Gaudi::Algorithm::sysExecute(EventContext const&) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/GAUDI/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libGaudiKernel.so)
==7535==    by 0x5832DFD8: AthSequencer::executeAlgorithm(Gaudi::Algorithm*, EventContext const&, bool volatile&, bool volatile&) const (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/Athena/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libGaudiSequencer.so)
==7535==    by 0x5832E448: AthSequencer::execute(EventContext const&) const (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/Athena/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libGaudiSequencer.so)
==7535==    by 0x1E26CC36: Gaudi::Algorithm::sysExecute(EventContext const&) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/GAUDI/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libGaudiKernel.so)
==7535==    by 0x326F90D6: AthenaEventLoopMgr::executeAlgorithms(EventContext const&) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/Athena/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libAthenaServices.so)
==7535==    by 0x326FF59B: AthenaEventLoopMgr::executeEvent(EventContext&&) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/Athena/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libAthenaServices.so)
==7535==    by 0x3270034E: AthenaEventLoopMgr::nextEvent(int) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/Athena/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libAthenaServices.so)
==7535==    by 0x326F9837: AthenaEventLoopMgr::executeRun(int) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/Athena/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libAthenaServices.so)
==7535==    by 0x2EA97E27: ApplicationMgr::executeRun(int) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/2019-08-21T2129/GAUDI/22.0.4/InstallArea/x86_64-centos7-gcc8-opt/lib/libGaudiCoreSvc.so)
==7535==    by 0x98FDF046: ???
==7535==    by 0xE3175E8: FastCall(long, void*, void*, void*) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/sw/lcg/releases/ROOT/6.18.00-2459b/x86_64-centos7-gcc8-opt/lib/libPyROOT.so)
==7535==    by 0xE3188FF: Cppyy::CallO(long, void*, void*, long) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/sw/lcg/releases/ROOT/6.18.00-2459b/x86_64-centos7-gcc8-opt/lib/libPyROOT.so)
==7535==    by 0xE31B9BD: PyROOT::TCppObjectByValueExecutor::Execute(long, void*, PyROOT::TCallContext*) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/sw/lcg/releases/ROOT/6.18.00-2459b/x86_64-centos7-gcc8-opt/lib/libPyROOT.so)
==7535==    by 0xE341C41: PyROOT::TMethodHolder::CallSafe(void*, long, PyROOT::TCallContext*) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/sw/lcg/releases/ROOT/6.18.00-2459b/x86_64-centos7-gcc8-opt/lib/libPyROOT.so)
==7535==    by 0xE340F04: PyROOT::TMethodHolder::Execute(void*, long, PyROOT::TCallContext*) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/sw/lcg/releases/ROOT/6.18.00-2459b/x86_64-centos7-gcc8-opt/lib/libPyROOT.so)
==7535==    by 0xE3405D7: PyROOT::TMethodHolder::Call(PyROOT::ObjectProxy*&, _object*, _object*, PyROOT::TCallContext*) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/sw/lcg/releases/ROOT/6.18.00-2459b/x86_64-centos7-gcc8-opt/lib/libPyROOT.so)
==7535==    by 0xE322080: PyROOT::(anonymous namespace)::mp_call(PyROOT::MethodProxy*, _object*, _object*) (in /cvmfs/atlas-nightlies.cern.ch/repo/sw/master/sw/lcg/releases/ROOT/6.18.00-2459b/x86_64-centos7-gcc8-opt/lib/libPyROOT.so)
==7535==    by 0x4E87322: PyObject_Call (abstract.c:2544)
```

So typically you only care about the top couple of lines.
The top most line is where the actual `malloc` happens, so that's not super relevant.
If you look one line below you'll see the actual line that caused the leak.
Here we can clearly see the source file and the line number.
However, for all the rest you only see the shared objects. 
The key here is that, the relevant code (in this case `TrkAmbiguitySolver`) 
was locally compiled but everthing else is taken from the `opt` nightly build.

In principle, we should worry about leaks that have a high byte count as well as high block count.
These are typically repetitive leaks in the event-loop which are very problematic.
Another give away is the calls from the event loop manager in the stack, of course :)

The reason for the leak depends on the problem at hand but it typically boils 
down to a heap allocation (`new`) that is not freed afterwards (`delete`).
In order the minimize the chances of causing a leak, try to minimize the heap allocations.
If not avoidable, stick to using `std::unique_ptr` (or at least `std::shared_ptr`). 
If you absolutely have to pass around bare pointers, make sure the ownership is clearly explained 
and always clean-up after yourself.

You can look through the log and try to figure out what other problems are there in this test.
There are more ;)
